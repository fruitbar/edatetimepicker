# EDateTimePicker

##About
This extension uses the jQuery plugin [DateTimePicker][dateTimePicker].
You can use it to pick a date and / or a time.
[dateTimePicker]: http://xdsoft.net/jqplugins/datetimepicker/
[dateTimePickerSrc]: https://github.com/xdan/datetimepicker
[EDateTimePicker]: https://bitbucket.org/fruitbar/edatetimepicker

##Requirements
Yii 1.1.16 or above (untested below)

##Usage
Add to your view:
```php
  Yii::import('ext.EDateTimePicker.EDateTimePicker');
    $this->widget('EDateTimePicker',array(
        'model'=>$model,
        'attribute'=>'attributeName',
        'options'=>array(
            'timepicker'=>true,
            'step'=>10,
            'format'=>'Y-m-d H:i',
            'dayOfWeekStart'=>0,
            'minDate'=>date('Y-m-d'),
            'yearStart'=>date('Y'),
        ),
        'htmlOptions'=>array(
          'maxlength'=>'16',
          'title'=>'Enter or select date (YYYY-MM-DD HH:MM)',
          'onchange'=>'javaScriptFunction(this)'
        ),
      )
    );
```

For a complete list of ```options``` please look into the [DateTimePicker Documentation][dateTimePicker].

##Resources
* [DateTimePicker Dokumentation][dateTimePicker]
* [DateTimePicker GitHub][dateTimePickerSrc]

##Project page
[EDateTimePicker][EDateTimePicker]
