<?php
/**
 * EDateTimePicker class file.
 *
 * @author Andreas Wenger <andreas.wenger@realstuff.ch>
 * @link http://www.realstuff.ch/
 * @copyright Copyright &copy; 2015 RealStuff Informatik AG
 */

class EDateTimePicker extends CInputWidget
{
  public $options=array();
  public $htmlOptions;
  public $language;
  
  protected function registerClientScript() {
    Yii::app()->clientScript->registerCoreScript('jquery');
    $cs=Yii::app()->clientScript;
    
    $assetPath = Yii::app()->getAssetManager()->publish(dirname(__FILE__).'/assets');;
    
    $cs->registerScriptFile($assetPath."/js/jquery.datetimepicker.full.min.js");
    $cs->registerCssFile($assetPath."/css/jquery.datetimepicker.css");
  }
  
  public function init() {
    $this->registerClientScript();
    return parent::init();
  }
  
  public function run() {
    list($name,$id)=$this->resolveNameID();

		if(isset($this->htmlOptions['id']))
			$id=$this->htmlOptions['id'];
		else
			$this->htmlOptions['id']=$id;
		if(isset($this->htmlOptions['name']))
			$name=$this->htmlOptions['name'];
		else
			$this->htmlOptions['name']=$name;
		
		if($this->hasModel())
			echo CHtml::activeTextField($this->model,$this->attribute,$this->htmlOptions);
		else
			echo CHtml::textField($name,$this->value,$this->htmlOptions);
    
    if (!isset($this->language))
      $this->language = Yii::app()->language;  
    
    Yii::app()->clientScript->registerScript("EDateTimePicker".$id, "
      $.datetimepicker.setLocale('".$this->language."');
      jQuery('#".$id."').datetimepicker(".CJSON::encode($this->options).");
    ");
  }
}